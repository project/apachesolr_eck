<?php

class ApachesolrEckIndexer {

  public static function __callStatic($name, $arguments) {
    if (substr($name, 0, 8) == 'reindex_') {
      $entity_type = substr($name, 8);
      $env_id = $arguments[0];
      // There could be another argument "bundle", but we'll leave it there for now.
      return static::reindex($entity_type, $env_id);
    }

    return NULL;
  }

  /**
   * Callback function for reindex.
   *
   * @param $entity_type
   * @param $env_id
   *
   * @return bool|null
   */
  private static function reindex($entity_type, $env_id) {
    if (! apachesolr_get_index_bundles($env_id, $entity_type)) {
      return NULL;
    }

    $indexer_table = apachesolr_get_indexer_table($entity_type);
    $transaction = db_transaction();
    try {
      db_delete($indexer_table)
        ->condition('entity_type', $entity_type)
        ->execute();

      $select = db_select('eck_' . $entity_type, 'e');
      $select->addExpression("'$entity_type'", 'entity_type');
      $select->addField('e', 'id', 'entity_id');
      $select->addField('e', 'type', 'bundle');
      $select->addExpression(REQUEST_TIME, 'changed');

      $db_insert = db_insert($indexer_table)
        ->fields(array('entity_id', 'bundle', 'entity_type', 'changed'))
        ->from($select);
      $db_insert
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog_exception('Apache Solr', $e);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Status callback.
   *
   * @param int $entity_id
   *   Entity id.
   * @param string $entity_type
   *   Entity type.
   *
   * @return bool
   *   TRUE if the entity is accessible (published), FALSE otherwise.
   */
  public static function status($entity_id, $entity_type) {
    return TRUE;
  }

  /**
   * Builds the user-specific information for a Solr document.
   *
   * @param ApacheSolrDocument $document
   *   The Solr document we are building up.
   * @param stdClass $entity
   *   The entity we are indexing.
   * @param string $entity_type
   *   The type of entity we're dealing with.
   *
   * @return array
   *   Array of documents.
   */
  public static function buildDocument(ApacheSolrDocument $document, $entity, $entity_type) {
    $document->label = apachesolr_clean_text($entity->title);

    $entities = array($entity);
    $build = entity_view($entity_type, $entities, 'search_index');
    // We only want pure text, not too many divs or hidden content.
    unset($build['#theme']);
    $text = drupal_render($build);
    $document->content = apachesolr_clean_text($text);
    $document->teaser = truncate_utf8($document->content, 300, TRUE);

    // Generic usecase for future reference. Callbacks can
    // allow you to send back multiple documents
    $documents = array();
    $documents[] = $document;
    return $documents;
  }

}
